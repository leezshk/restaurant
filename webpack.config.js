const HtmlWebPackPlugin = require("html-webpack-plugin");
const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/app/index.html",
    filename: "./index.html"
});

module.exports = {
    entry: __dirname + '/src/app/index.js',

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                loader:[ 'style-loader', 'css-loader' ]
            }
        ]
    },
    plugins: [htmlPlugin]
};