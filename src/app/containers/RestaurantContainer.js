import {connect} from 'react-redux';
import Restaurant from "../components/Restaurant";
import {setSortFilter, Sort, updateList} from "../actions/RestaurantAction";
import RestaurantAPI from "../api/RestaurantAPI";

const mapStateToProps = (state) => {
    return{
        restaurantList: state.restaurant.restaurantList,
        country_id: state.restaurant.country_id,
        city_id: state.restaurant.city_id,
        sort_by: state.restaurant.sort_by,
        cityList: state.restaurant.cityList
    }
};

const mapDispatchToProps = (dispatch, state) => {
    return{
        onFetchRestaurantList: (payload, onUpdateList) => {
            RestaurantAPI.getRestaurantList(onUpdateList, payload);
        },

        handleSort: (payload, onUpdateList) => {
            let sortOrder;
            switch (payload.sort_by) {
                case 1 :
                    sortOrder = 'asc';
                    dispatch(setSortFilter(Sort.ASC));
                    break;
                case 2:
                    sortOrder = 'desc';
                    dispatch(setSortFilter(Sort.DESC));
                    break;
                case 3:
                    sortOrder = 'rating';
                    dispatch(setSortFilter(Sort.RATING));
                    break;
            }
            RestaurantAPI.sort({country_id: payload.country_id, city_id: payload.city_id, sort_by: sortOrder}, onUpdateList);
        } ,

        handleSearch: (payload, onUpdateList) => {
            let text = document.getElementById('text').value;
            RestaurantAPI.selectCity({country_id: payload.country_id, city_id: payload.city_id, sort_by: payload.sort_by}, onUpdateList);
        },

        handleCity: (payload, onUpdateList) => {
            RestaurantAPI.selectCity({country_id: payload.country_id, city_id: payload.city_id, sort_by: payload.sort_by}, onUpdateList);
        },

        onUpdateList: (response) => {
            dispatch(updateList(response));
        },
    }
};

const RestaurantContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Restaurant);

export default RestaurantContainer;