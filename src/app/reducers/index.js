import restaurant from './RestaurantReducer';
import {combineReducers} from "redux";

const AppReducer = combineReducers({
    restaurant
});

export default AppReducer;