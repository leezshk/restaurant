import {GET_RESTAURANT, SET_SORT_FILTER, Sort, UPDATE_LIST} from "../actions/RestaurantAction";

const initialState = {
    country_id: 52,
    city_id: 166,
    restaurantList: [
        {
            name: 'Bajeko Sekuwa',
            address: 'Kathmandu',
            image: 'http://api.stagingapp.io/cdn/v1/images/7991107285a1a543a2c63d.png'
        },
        {
            name: 'French Bakery',
            address: 'Paknajol',
            image: 'http://api.stagingapp.io/cdn/v1/images/7991107285a1a543a2c63d.png'
        },
        {
            name: 'Burger House',
            address: 'Thamel',
            image: 'http://api.stagingapp.io/cdn/v1/images/7991107285a1a543a2c63d.png'
        }
    ],
    sort_by: Sort.ASC,
    cityList: [
        {
            name: "Kathmandu",
            id: 12
        },
        {
            name: "Pokhara",
            id: 19
        }
    ]
};

function restaurant(state = initialState, action) {
    switch(action.type){
        case GET_RESTAURANT:
            console.log("reducer: ", action.response);
            return state;

        case SET_SORT_FILTER:
            console.log("reducer: ", action.filter);
            return Object.assign({}, state, {
                sort_by: action.filter,
            });

        case UPDATE_LIST:
            return Object.assign({}, state, {
                restaurantList: action.response,
                // city_id: city_id set city id according to response
            });
        default:
            return state;
    }
}

export default restaurant;