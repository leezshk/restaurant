export const GET_RESTAURANT = 'getRestaurant';
export const SET_SORT_FILTER = 'setSortFilter';
export const UPDATE_LIST = 'updateList';

export function getRestaurant(response){
    return{
        type: GET_RESTAURANT,
        response: response
    }
}

export function setSortFilter(filter){
    console.log("Action:", filter);
    return{
        type: SET_SORT_FILTER,
        filter
    }
}

export function updateList(response){
    return{
        type: UPDATE_LIST,
        response: response
    }
}
export const Sort = {
    ASC : "asc",
    DESC: "desc",
    RATING: "rating"
};