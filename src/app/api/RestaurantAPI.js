const BASE_URL = 'http://api.stagingapp.io';

const API_ENDPOINTS = {
    GET_RESTAURANT_LIST: BASE_URL + '/restaurant/v1/search/branch/',
    GET_CITY_LIST: BASE_URL + '/location/v1/public/country/52/city'
};

const APIHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Origin': '',
};

class RestaurantAPI {
    getRestaurantList(callback, payload) {
        return fetch(API_ENDPOINTS.GET_RESTAURANT_LIST + '?country_id=' + payload.country_id + '&city_id=' + payload.city_id , {
            method: 'GET',
            headers: APIHeaders
        })
        .then((res) => {
        return res.json()
        })
        .then(response => {
            console.log("res" + response);
            callback(response);
        })
        .catch(error => {
            console.log("error" + error);
        });
    }

    search(callback, payload){
        return fetch(API_ENDPOINTS.GET_RESTAURANT_LIST + '?country_id=' + payload.country_id + '&city_id=' + payload.city_id , {
            method: 'GET',
            headers: APIHeaders
        }).then((res) => {
                return res.json()
            })
            .then(response => {
                console.log("res" + response);
                callback(response);
            })
            .catch(error => console.log("error" + error));
    }

    sort(callback, payload){
        return fetch(API_ENDPOINTS.GET_RESTAURANT_LIST
            + '?country_id=' + payload.country_id + '&city_id=' + payload.city_id + '&sort=' + payload.sort_by
            , {
            method: 'GET',
            headers: APIHeaders
        }).then((res) => {
                return res.json()
            })
            .then(response => {
                console.log("res" + response);
                callback(response);
            })
            .catch(error => console.log("error" + error));
    }

    selectCity(callback, payload){
        return fetch(API_ENDPOINTS.GET_RESTAURANT_LIST
            + '?country_id=' + payload.country_id + '&city_id=' + payload.city_id + '&sort=' + payload.sort_by
            , {
                method: 'GET',
                headers: APIHeaders
            }).then((res) => {
            return res.json()
        })
            .then(response => {
                console.log("res" + response);
                callback(response);
            })
            .catch(error => console.log("error" + error));
    }
}

export default new RestaurantAPI();