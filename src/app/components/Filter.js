import React from 'react';

const Filter = ({ handleSearch, handleSort, cityList, country_id, city_id, onUpdateList, sort_by, handleCity}) => {
    return(
        <div className="filter-container">
            <div className="header">Filter</div>

            <div className="search-form">
                <div className="input-box">
                    <span><input type="text" className="input-text" id="text"/></span>
                    <span ><i className="material-icons" onClick={handleSearch}>search</i></span>
                </div>
            </div>

            <div className="sort-box">
                <div className="title">Sort by</div>
                <div id="sort">
                    <input type="radio" id="asc" checked="checked" onChange={handleSort.bind(this,
                        {country_id: country_id, city_id: city_id, sort_by: 1}, onUpdateList)}
                           name="sort" value="asc"/>
                    <label htmlFor="asc">A-Z</label>
                </div>


                <div id="sort">
                    <input type="radio" id="desc" onChange={handleSort.bind(this,
                        {country_id: country_id, city_id: city_id, sort_by: 2}, onUpdateList)}
                           name="sort" value="desc"/>
                    <label htmlFor="desc">Z-A</label>
                </div>

                <div id="sort">
                    <input type="radio" id="rating" onChange={handleSort.bind(this,
                        {country_id: country_id, city_id: city_id, sort_by: 3}, onUpdateList )}
                           name="sort" value="rating"/>
                    <label htmlFor="rating">Rating</label>
                </div>
            </div>

            <div className="city-box">
                <div className="title">City</div>

                {cityList.map(function (city, id) {
                    return(
                        <div className="city-list">
                            <span><input type="radio" id="rating" name="city" value={city.name}
                            onChange={handleCity.bind(this, {country_id: country_id, city_id: city.id, sort_by: sort_by} )}/></span>
                            <span>{city.name}</span>
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default Filter;