import React from 'react';
import styles from '../css/app.css';
import RestaurantContainer from "../containers/RestaurantContainer";

class App extends React.Component {
    render(){
        return(
            <RestaurantContainer />
        )
    }
}

export default App;
