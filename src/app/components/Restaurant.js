import React from 'react';
import PropTypes from 'prop-types';
import RestaurantList from "./RestaurantList";
import Filter from "./Filter";

const Restaurant = ({country_id, city_id, onFetchRestaurantList, restaurantList, cityList
                        , handleSearch, handleSort, sort_by, onUpdateList, handleCity}) => {

    onFetchRestaurantList({country_id: country_id,city_id: city_id}, onUpdateList);
    return(
        <div className="main-container">
            <Filter cityList={cityList} sort_by={sort_by}
                    handleSearch={handleSearch} handleSort={handleSort} handleCity={handleCity}
                    country_id={country_id} city_id={city_id} onUpdateList={onUpdateList}
            />
            <div className="restaurant-container">
                <RestaurantList restaurantList={restaurantList}/>
            </div>
        </div>
    );
};

Restaurant.propTypes = {
    country_id: PropTypes.number,
    city_id:PropTypes.number,
    onFetchRestaurantList:PropTypes.func,
    onUpdateList:PropTypes.func,
    restaurantList:PropTypes.array,
    cityList:PropTypes.array,
    handleSearch: PropTypes.func,
    handleSort:PropTypes.func,
    handleCity: PropTypes.func
};

export default Restaurant;