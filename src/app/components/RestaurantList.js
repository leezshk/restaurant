import React from 'react';
import PropTypes from 'prop-types';

const RestaurantList = ({restaurantList}) => {
    return (
        restaurantList.map(function (restaurant, id) {
            return (
                <div className="list-container">
                    <div className="left-container">
                        <div className="restaurant-logo">
                            <img src={restaurant.image} width={100}
                                 height={100}/>
                        </div>

                        <div className="restaurant-name">
                            <div>
                                <span>{restaurant.name}</span>
                                <span className="open">open</span>
                            </div>
                            <div>{restaurant.address}</div>
                        </div>
                    </div>
                    <div className="right-container">
                        <span>rating</span>
                        <span>4 reviews</span>
                        <span className="view-box">View</span>
                    </div>
                </div>
            )
        })
    )
};

// const RestaurantList = ({country_id, city_id, onFetchRestaurantList, restaurantList}) => {
//     console.log("con", city_id);
//
//     onFetchRestaurantList({country_id: country_id,city_id: city_id});
//     console.log("lst", restaurantList);
//
//     return (
//         restaurantList.map(function (restaurant, id) {
//             return (
//                 <div className="list-container">
//                     <div className="left-container">
//                         <div className="restaurant-logo">
//                             <img src={restaurant.image} width={100}
//                                  height={100}/>
//                         </div>
//
//                         <div className="restaurant-name">
//                             <div>
//                                 <span>{restaurant.name}</span>
//                                 <span className="open">open</span>
//                             </div>
//                             <div>{restaurant.address}</div>
//                         </div>
//                     </div>
//                     <div className="right-container">
//                         <span>rating</span>
//                         <span>4 reviews</span>
//                         <span className="view-box">View</span>
//                     </div>
//                 </div>
//             )
//         })
//     )
// };
//
// RestaurantList.propTypes = {
//     country_id: PropTypes.number,
//     city_id:PropTypes.number,
//     onFetchRestaurantList:PropTypes.func,
//     restaurantList:PropTypes.array
// };

export default RestaurantList;